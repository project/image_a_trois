<?php
/**
 * @file
 * Context Reaction class to add an Image-a-trois overlay.
 */

/**
 * Expose Image-a-trois overlays as context reactions.
 */
class image_a_trois_context_reaction_add_overlay extends context_reaction {

  /**
   * Allow admins to select which overlay to use as a reaction.
   */
  public function options_form($context) {
    $overlays = ctools_export_crud_load_all('image_a_trois_overlays');
    $options = array();
    foreach ($overlays as $overlay) {
      $options[$overlay->name] = $overlay->description;
    }
    ksort($options);

    return array(
      '#title' => check_plain($this->title),
      '#description' => check_plain($this->description),
      '#options' => $options,
      '#type' => 'select',
      '#default_value' => $this->fetch_from_context($context),
    );
  }

  /**
   * Add the relevant Image-a-trois overlay.
   *
   * @see image_a_trois_preprocess_page()
   */
  public function execute() {
    $contexts = $this->get_contexts();
    foreach ($contexts as $context) {
      if (!empty($context->reactions[$this->plugin])) {
        ctools_include('export');
        $overlay = ctools_export_crud_load('image_a_trois_overlays', $context->reactions[$this->plugin]);
        image_a_trois_overlay_init($overlay);
      }
    }
  }
}
