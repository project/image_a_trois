<?php
/**
 * @file
 * Plugin definition for ctools export-ui
 */

$plugin = array(
  'schema' => 'image_a_trois_overlays',
  'access' => 'administer image a trois',
  'menu' => array(
    'menu prefix' => 'admin/structure',
    'menu item' => 'image-a-trois-overlays',
    'menu title' => 'Image-a-trois overlays',
    'menu description' => 'Administer image a trois overlays.',
  ),
  // These are required to provide proper strings for referring to the
  // actual type of exportable. "proper" means it will appear at the
  // beginning of a sentence.
  'title singular' => t('overlay'),
  'title singular proper' => t('Overlay'),
  'title plural' => t('overlays'),
  'title plural proper' => t('Overlays'),
  'form' => array(
    'settings' => 'image_a_trois_ctools_export_ui_form',
  ),
);

/**
 * Define the preset add/edit form.
 */
function image_a_trois_ctools_export_ui_form(&$form, &$form_state) {
  $overlay = $form_state['item'];

  $form['info']['name']['#type'] = 'machine_name';
  $form['info']['name']['#machine_name'] = array(
    'exists' => 'image_a_trois_overlay_exists',
    'source' => array(
      'info',
      'description',
    ),
  );

  $form['info']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Overlay name'),
    '#id' => 'overlay-description',
    '#description' => t('The human readable name or description of this preset.'),
    '#default_value' => $overlay->description,
    '#required' => TRUE,
    '#weight' => -1,
  );

  $form['info']['image_url'] = array(
    '#title' => t('Image url'),
    '#type' => 'textfield',
    '#description' => t('A relative or absolute url to the image for this overlay.'),
    '#default_value' => $overlay->image_url,
  );

  $form['info']['paths'] = array(
    '#title' => t('Overlay paths'),
    '#type' => 'textarea',
    '#description' => t('The internal paths this overlay should be used on. One path per line and &lt;front&gt; is the home page.'),
    '#default_value' => $overlay->paths,
  );
}
