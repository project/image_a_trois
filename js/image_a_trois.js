(function($){
  Drupal.behaviors.image_a_trois = {
    attach: function(context, settings) {
      // We use once() to ensure that we don't attempt to duplicate overlays
      // when attachBehaviors is called multiple times on a single page load,
      // like after every ajax response.
      $('body').once('image_a_trois', function() {
        // @TODO: This could be a setting on an admin page somewhere.
        iOpts = {
          aligned: 'center'
        };

        i = 0;
        $.each(settings.image_a_trois, function(key, val) {
          // We simply pass the first 3 images straight through to the plugin.
          if (i < 3) {
            $.image_a_trois(val, iOpts);
          }
          i++;
        });
      });
    }
  };
})(jQuery);
