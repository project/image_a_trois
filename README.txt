Image-a-trois library can be found at http://image-a-trois.frebsite.nl/

The library git reposity is hosted on github
at https://github.com/thedavidmeister/jquery.ia3

To install the jQuery library download and extract it to
sites/all/libraries/jquery.ia3

Alternatively, add the following to your make file:

libraries[ia3][download][type] = "git"
libraries[ia3][download][url] = "git@github.com:thedavidmeister/jquery.ia3.git"
libraries[ia3][directory_name] = "jquery.ia3"

If you have the Libraries module installed you can place the jquery.ia3 folder
in any valid location scanned by Libraries and it will be automatically
detected and included.

Once you have included the jQuery library you can create and manage
Image-a-trois overlays at admin/structure/image-a-trois-overlays. You will need
the "Administer Image-a-trois" permission to access this page.

If you have the Context module enabled you can configure existing Image-a-trois
overlays to be displayed as Context reactions.

If you have installed the module correctly and can't see any overlays, check
that your user has the "View Image-a-trois overlays" permission.

The image_a_trois jQuery plugin was written and copyrighted by Fred Heusschen
under a dual MIT and GPL license. His website is http://www.frebsite.nl
