<?php
/**
 * @file
 * Makes Image-a-trois overlays available as a Context Reaction.
 */

/**
 * Implements hook_context_plugins().
 */
function image_a_trois_context_plugins() {
  $plugins = array();
  $plugins['image_a_trois_context_reaction_add_overlay'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'image_a_trois') . '/plugins/context_reaction',
      'file' => 'image_a_trois_context_reaction_add_overlay.inc',
      'class' => 'image_a_trois_context_reaction_add_overlay',
      'parent' => 'context_reaction',
    ),
  );
  return $plugins;
}

/**
 * Implements hook_context_registry().
 */
function image_a_trois_context_registry() {
  return array(
    'reactions' => array(
      'image_a_trois_context_reaction_add_overlay' => array(
        'title' => t('Image-a-trois overlay'),
        'plugin' => 'image_a_trois_context_reaction_add_overlay',
      ),
    ),
  );
}

/**
 * Implements hook_context_page_reaction().
 */
function image_a_trois_context_page_reaction() {
  if ($plugin = context_get_plugin('reaction', 'image_a_trois_context_reaction_add_overlay')) {
    $plugin->execute();
  }
}
